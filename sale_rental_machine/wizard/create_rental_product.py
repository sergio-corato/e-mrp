# -*- encoding: utf-8 -*-
##############################################################################
#
from openerp import models, fields, api, _


class CreateRentalProduct(models.TransientModel):
    _inherit = 'create.rental.product'

    minimum_rental_days = fields.Integer()
    maximum_rental_days = fields.Integer()
    name = fields.Char(size=256)

    @api.model
    def _prepare_rental_product(self):
        vals = super(CreateRentalProduct, self)._prepare_rental_product()
        vals['minimum_rental_days'] = self.minimum_rental_days
        vals['maximum_rental_days'] = self.maximum_rental_days
        vals['property_account_income'] = vals['property_account_expense'] \
            = False
        return vals
