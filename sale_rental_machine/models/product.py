# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    minimum_rental_days = fields.Integer()
    maximum_rental_days = fields.Integer()


class ProductCategory(models.Model):
    _inherit = 'product.category'

    rental_exclude_sunday = fields.Boolean()
    rental_exclude_saturday = fields.Boolean()
    rental_exclude_holiday = fields.Boolean()
