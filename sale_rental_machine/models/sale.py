# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _
from openerp.exceptions import ValidationError
from dateutil.relativedelta import relativedelta


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    # @api.multi
    # def _get_hours_machine(self):
    #     for line in self.filtered('rental'):
    #         line_out = self.env['sale.order.line'].search([
    #             ('product_id', '=', line.product_id),
    #             ('id', '!=', line.id),
    #             ('state', '=', 'done'),
    #         ], limit=1, order='end_date desc')
    #         if line_out:
    #             line.hours_out = line_out.hours_in
            # else:
            #     line.hours_out = #get hours from machine

    end_date = fields.Date(
        string='End Date', readonly=True,
        states={'draft': [('readonly', False)], 'confirmed': [('readonly', False)]})
    out_state = fields.Selection([
        ('draft', 'New'),
        ('cancel', 'Cancelled'),
        ('waiting', 'Waiting Another Move'),
        ('confirmed', 'Waiting Availability'),
        ('assigned', 'Available'),
        ('done', 'Done'),
    ], readonly=True, related='sell_rental_id.out_state',
        string='State of the Outgoing Stock Move')
    partner_id = fields.Many2one(
        related='order_id.partner_id',
        string="Partner", readonly=True)
    in_state = fields.Selection([
        ('draft', 'New'),
        ('cancel', 'Cancelled'),
        ('waiting', 'Waiting Another Move'),
        ('confirmed', 'Waiting Availability'),
        ('assigned', 'Available'),
        ('done', 'Done'),
        ], readonly=True, related='sell_rental_id.in_state',
        string='State of the Return Stock Move')
    # hours_out = fields.Integer('Hours on outgoing', compute=_get_hours_machine)
    hours_in = fields.Integer('Hours on incoming')

    @api.one
    @api.constrains(
        'rental_type', 'extension_rental_id', 'start_date', 'end_date',
        'rental_qty', 'product_uom_qty', 'product_id', 'must_have_dates')
    def _check_sale_line_rental(self):
        if self.rental_type in ('new_rental', 'rental_extension'):
            # todo check availability by days in sale.order.lines and not
            # todo warehouse moves, we can rent in the future
            # tried only for duplication line, not needed i think: '&', ('state', 'not in', ['cancel', 'draft', 'sent']),
            existing_rental_ids = self.search([
                '&', ('id', '!=', self.id),
                '&', ('product_id.rented_product_id', '=',
                      self.product_id.rented_product_id.id),
                '&', ('order_id.state', 'not in', ['cancel', 'draft', 'sent']),
                '|',
                '&',
                    ('start_date', '<=', self.start_date),
                    ('end_date', '>=', self.start_date),
                '&',
                    ('start_date', '<=', self.end_date),
                    ('end_date', '>=', self.end_date),
            ])
            if existing_rental_ids:
                raise ValidationError(
                    _('Already rented machine!')
                )

    @api.onchange('start_date')
    def start_date_change(self):
        if self.start_date:
            if self.end_date and self.start_date > self.end_date:
                self.end_date = self.start_date
            if self.end_date:
                number_of_days = self.compute_rental_days(
                    self.end_date,
                    self.start_date,
                    self.product_id.categ_id,
                )
                if self.number_of_days != number_of_days:
                    self.number_of_days = number_of_days

    @api.onchange('end_date')
    def end_date_change(self):
        if self.end_date:
            if self.start_date and self.start_date > self.end_date:
                self.start_date = self.end_date
            if self.start_date:
                number_of_days = self.compute_rental_days(
                    self.end_date,
                    self.start_date,
                    self.product_id.categ_id,
                )
                if self.number_of_days != number_of_days:
                    self.number_of_days = number_of_days

    @api.multi
    def compute_rental_days(self, date_to, date_from, ctg_id=False):
        days = (fields.Date.from_string(date_to) -
                fields.Date.from_string(date_from)).days + 1
        if date_from and date_to and ctg_id:
            date_from = fields.Date.from_string(date_from)
            date_to = fields.Date.from_string(date_to)
            date_dt = date_from
            while date_dt <= date_to:
                # if public holiday or not working day skip count
                if ctg_id.rental_exclude_saturday and date_dt.isoweekday() \
                        == 6 or ctg_id.rental_exclude_sunday and \
                        date_dt.isoweekday() == 7 or \
                        ctg_id.rental_exclude_holiday and \
                        self.env['hr.holidays.public.line'].search(
                        [('date', '=', date_dt)]):
                    days -= 1
                date_dt += relativedelta(days=1)
        return days

    @api.onchange('number_of_days', 'start_date', 'end_date', 'discount')
    def number_of_days_change(self):
        # select correct type of service on duration
        if self.product_id.rented_product_id.rental_service_ids:
            rental_service_id = self.product_id.rented_product_id.\
                rental_service_ids.filtered(
                    lambda x: x.maximum_rental_days >= self.number_of_days >=
                    x.minimum_rental_days
                )
            if rental_service_id:
                self.product_id = rental_service_id[0].id
                self.product_uom = rental_service_id[0].uom_id.id
                self.price_unit = self.product_id.list_price
                if not self.rental_type:
                    self.rental_type = 'new_rental'
                if not self.name:
                    self.name = self.product_id.name
                if self.rental_qty == 0:
                    self.rental_qty = 1
                self.product_uom_qty = self.number_of_days * self.rental_qty
                self.price_subtotal = self.product_uom_qty * self.price_unit *(
                    100 - self.discount)/100.0
                # verificare se riutilizzare l'onchange del sale rental

    # @api.onchange('product_id') # mmmm, sembra ci sia ma non fa nulla
    # def onchange_product_id(self):
    # intanto risolto come sopra, ma non lo corregge se lo cambio dopo ovviamente
    # TODO l'onchange sul rental non va qui, forse per questo? MM, NO VA, mancava
    # solo il campo da poter aggiornare (giorni di noleggio), messo

    # override original method in sale_start_end_date to check recomputed days
    @api.one
    @api.constrains('start_date', 'end_date', 'number_of_days')
    def _check_start_end_dates(self):
        if self.product_id and self.must_have_dates:
            if not self.end_date:
                raise ValidationError(
                    _("Missing End Date for sale order line with "
                        "Product '%s'.")
                    % (self.product_id.name))
            if not self.start_date:
                raise ValidationError(
                    _("Missing Start Date for sale order line with "
                        "Product '%s'.")
                    % (self.product_id.name))
            if not self.number_of_days:
                raise ValidationError(
                    _("Missing number of days for sale order line with "
                        "Product '%s'.")
                    % (self.product_id.name))
            if self.start_date > self.end_date:
                raise ValidationError(
                    _("Start Date should be before or be the same as "
                        "End Date for sale order line with Product '%s'.")
                    % (self.product_id.name))
            if self.number_of_days < 0:
                raise ValidationError(
                    _("On sale order line with Product '%s', the "
                        "number of days is negative ; this is not allowed.")
                    % (self.product_id.name))
            days_delta = self.compute_rental_days(
                    self.end_date,
                    self.start_date,
                    self.product_id.categ_id)
            if self.number_of_days != days_delta:
                raise ValidationError(
                    _("On the sale order line with Product '%s', "
                        "there are %d days between the Start Date (%s) and "
                        "the End Date (%s), but the number of days field "
                        "has a value of %d days.")
                    % (self.product_id.name, days_delta, self.start_date,
                        self.end_date, self.number_of_days))

    @api.multi
    def button_rental_draft(self):
        orders = self.mapped('order_id')
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order',
            'view_mode': 'form,tree',
            'res_id': orders[0].id,
            'target': 'current',
        }

    @api.model
    def create(self, vals):
        # if rental service line, create order automatically
        if 'order_id' not in vals and 'order_partner_id' in vals:
            if 'product_id' in vals:
                product = self.env['product.product'].browse(
                    vals['product_id'])
                if product.rented_product_id:
                    vals['must_have_dates'] = True
                    # create order
                    order_id = self.env['sale.order'].create({
                        'partner_id': vals['order_partner_id'],
                    })
                    if order_id:
                        vals['order_id'] = order_id.id
                        # add taxes
                        if not vals.get('tax_id', False):
                            fpos = order_id.fiscal_position if \
                                order_id.fiscal_position else \
                                order_id.partner_id.property_account_position
                            if fpos:
                                taxes_ids = fpos.map_tax(product.taxes_id)
                            else:
                                taxes_ids = product.taxes_id.filtered(
                                    lambda x: x.company_id ==
                                    order_id.company_id)
                            vals['tax_id'] = [(6, 0, taxes_ids.ids)]

                    #  todo set state machine to offered (disregarding dates)
                    offered_stage_id = self.env['machinery.stage'].search([
                        ('state', '=', 'offered')])[0]
                    available_stage_id = self.env['machinery.stage'].search([
                        ('state', '=', 'available')])[0]
                    machine_id = self.env['machinery'].search([(
                        'product', '=', product.rented_product_id.id)])
                    if machine_id.stage_id == available_stage_id:
                        machine_id.stage_id = offered_stage_id

        res = super(SaleOrderLine, self).create(vals)
        return res

    @api.model
    def _prepare_order_line_invoice_line(self, line, account_id=False):
        res = super(SaleOrderLine, self)._prepare_order_line_invoice_line(
            line, account_id=account_id)
        if line.must_have_dates and line.product_id.rented_product_id:
            machine_id = self.env['machinery'].search([(
                'product', '=', line.product_id.rented_product_id.id)])
            if machine_id:
                if machine_id.account_id:
                    res.update({'account_id': machine_id.account_id.id})
                else:
                    raise ValidationError(
                        _('Missing income account in machine %s!'
                        % line.product_id.rented_product_id.name))
        return res

    @api.onchange('extension_rental_id')
    def extension_rental_id_change(self):
        super(SaleOrderLine, self).extension_rental_id_change()
        if (
                self.product_id and
                self.rental_type == 'rental_extension' and
                self.extension_rental_id):
            self.order_partner_id = self.extension_rental_id.partner_id


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_button_confirm(self):
        # set machine stage
        res = super(SaleOrder, self).action_button_confirm()
        for order in self:
            rented_stage_id = self.env['machinery.stage'].search([
                ('state', '=', 'rented')])[0]
            reserved_stage_id = self.env['machinery.stage'].search([
                ('state', '=', 'reserved')])[0]

            for line in order.order_line:
                machine_id = self.env['machinery'].search([
                    ('product', '=', line.product_id.rented_product_id.id)])
                if machine_id and line.rental_type:
                    if line.start_date <= fields.Date.today() <= line.end_date:
                        machine_id.stage_id = rented_stage_id
                    if line.start_date > fields.Date.today():
                        machine_id.stage_id = reserved_stage_id
        return res

    #todo if cancel or unlink or set to draft order, change stage machine

    def _preparare_ddt_data(self, cr, uid, order, context=None):
        res = super(SaleOrder, self)._preparare_ddt_data(
            cr, uid, order, context)
        picking_ids = [p.id for p in order.picking_ids]
        for pick in order.picking_ids:
            for line in pick.move_lines:
                if line.location_dest_id.id \
                        == self.pool['ir.model.data'].get_object_reference(
                        cr, uid, 'sale_rental',
                        'rental_out_stock_location')[1]:
                    ddt_type_id = self.pool[
                        'ir.model.data'].get_object_reference(
                            cr, uid, 'stock_ddt_data',
                            'ddt_type_ddt_noleggio')[1]
                    res['ddt_type_id'] = ddt_type_id
                    ddt_type = self.pool['stock.ddt.type'].browse(
                        cr, uid, ddt_type_id, context=context)
                    res['carriage_condition_id'] = \
                        ddt_type.carriage_condition_id.id
                    res['goods_description_id'] = \
                        ddt_type.goods_description_id.id
                    res['transportation_reason_id'] = \
                        ddt_type.transportation_reason_id.id
                    res['transportation_method_id'] = \
                        ddt_type.transportation_method_id.id
                    break
                if line.location_dest_id.id \
                        == self.pool['ir.model.data'].get_object_reference(
                            cr, uid, 'sale_rental',
                            'rental_in_stock_location')[1]:
                    picking_ids.remove(pick.id)
                    break
        res['picking_ids'] = [(6, 0, picking_ids)]
        return res
