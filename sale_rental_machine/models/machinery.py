# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _


class Machinery(models.Model):
    _inherit = "machinery"

    @api.multi
    def _get_sale_order_line_rental_ids(self):
        for machine in self:
            machine.sale_order_line_rental_ids = self.env[
                'sale.order.line'].search([
                    ('product_id.rented_product_id', '=', machine.product.id)])

    @api.multi
    def action_get_sale_rental_order_line(self):
        action_id = self.env['ir.model.data'].get_object_reference(
            'sale_rental_machine', 'sale_rental_order_line_action')[1]
        action = dict(
            name='Rentals',
            type='ir.actions.act_window',
            res_model='sale.order.line',
            view_mode='calendar,form,tree',
            target='new',
            flags={'form': {'action_buttons': True}},
            res_id=action_id,
            context={'default_res_model': self._name,
                     'default_res_id': self.id,
                     'default_product_id': self.product.rental_service_ids[0].\
                         id if self.product.rental_service_ids else False},
            domain=[('id', 'in', self.sale_order_line_rental_ids.ids)],
        )
        return action

    is_rentable = fields.Boolean()
    stage_id = fields.Many2one(
        'machinery.stage', 'Stage',
        select=True,
        default=lambda self: self.env['machinery.stage'].get_default(),
        copy=False)
    sale_order_line_rental_ids = fields.One2many(
        comodel_name='sale.order.line',
        compute='_get_sale_order_line_rental_ids',
    )

    _sql_constraints = [
        ('product_machine_uniq', 'unique(product)',
         'Product must be unique for machine!!'),
    ]

    @api.cr_uid
    def update_machine_state(self, cr, uid, ids=None, context=None):
        if not ids:
            ids = self.pool['machinery'].search(cr, uid, [])
        # verified = self.get_param('mailgun.verified')
        # if verified:
        #     return
        for machine in self.pool['machinery'].browse(cr, uid, ids, context):
            # check sale order line ids to update state
            # 1. check if is rented today to set 'rented'
            # 2. check if is rented in next days to set 'offered'
            rented_stage_id = self.pool['machinery.stage'].search(cr, uid, [
                ('state', '=', 'rented')
            ], context)[0]
            reserved_stage_id = self.pool['machinery.stage'].search(cr, uid, [
                ('state', '=', 'reserved')
            ], context)[0]
            offered_stage_id = self.pool['machinery.stage'].search(cr, uid, [
                ('state', '=', 'offered')
            ], context)[0]
            available_stage_id = self.pool['machinery.stage'].search(cr, uid, [
                ('state', '=', 'available')
            ], context)[0]
            machine.stage_id = rented_stage_id if \
                machine.sale_order_line_rental_ids.filtered(
                    lambda x: x.start_date <= fields.Date.today() <= x.end_date
                    and x.order_id.state not in ['cancel', 'draft', 'sent']
                ) else reserved_stage_id if \
                machine.sale_order_line_rental_ids.filtered(
                    lambda x: x.start_date > fields.Date.today()
                    and x.order_id.state not in ['cancel', 'draft', 'sent']
                ) else offered_stage_id if \
                machine.sale_order_line_rental_ids.filtered(
                    lambda x: x.start_date > fields.Date.today()
                    and x.order_id.state in ['draft', 'sent']
                ) else available_stage_id


class MachineryStage(models.Model):
    _name = "machinery.stage"
    _description = "Stage of machine"
    _rec_name = 'name'
    _order = "sequence"

    name = fields.Char(required=True, translate=True)
    sequence = fields.Integer(default=1)
    case_default = fields.Boolean(
        string='Default to New Machine',
        help="If you check this field, this stage will be proposed by default"
             " on each machine. It will not assign this stage to existing "
             "machines.")
    fold = fields.Boolean(
        string='Folded in Kanban View',
        default=False,
        help='This stage is folded in the kanban view when'
        'there are no records in that stage to display.')
    description = fields.Char()
    state = fields.Selection(
        selection=[
            ('available', 'Available'),
            ('offered', 'Offered'),
            ('reserved', 'Reserved'),
            ('rented', 'Rented'),
            ('repair', 'Repair'),
        ], string='State', required=True, default='available',
        help='State of machine in this stage.'
    )

    @api.model
    def get_default(self):
        return self.search([('case_default', '=', True)], limit=1).id
