# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp.osv import fields, osv


class mrp_repair(osv.osv):
    _name = 'mrp.repair'
    _inherit = "mrp.repair"

    def _amount_tax(self, cr, uid, ids, field_name, arg, context=None):
        """ Calculates taxed amount.
        @param field_name: Name of field.
        @param arg: Argument
        @return: Dictionary of values.
        """
        if context is None:
            context = {}
        res = super(mrp_repair, self)._amount_tax(
            cr, uid, ids, field_name, arg, context)
        #  return {}.fromkeys(ids, 0)
        cur_obj = self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        for repair in self.browse(cr, uid, ids, context=context):
            val = 0.0
            cur = repair.pricelist_id.currency_id
            for line in repair.operations:
                #  manage prices with tax included use compute_all instead of
                #  compute
                if line.to_invoice:
                    tax_calculate = tax_obj.compute_all(
                        cr, uid, line.tax_id,
                        line.price_unit * (1 - (line.discount or 0.0) / 100.0),
                        line.product_uom_qty, line.product_id,
                        repair.partner_id)
                    for c in tax_calculate['taxes']:
                        val += c['amount']
            for line in repair.fees_lines:
                if line.to_invoice:
                    tax_calculate = tax_obj.compute_all(
                        cr, uid, line.tax_id,
                        line.price_unit * (1 - (line.discount or 0.0) / 100.0),
                        line.product_uom_qty, line.product_id,
                        repair.partner_id)
                    for c in tax_calculate['taxes']:
                        val += c['amount']
            res[repair.id] = cur_obj.round(cr, uid, cur, val)
        return res

    def _get_lines(self, cr, uid, ids, context=None):
        return self.pool['mrp.repair'].search(cr, uid, [
            ('operations', 'in', ids)], context=context)

    def _get_fee_lines(self, cr, uid, ids, context=None):
        return self.pool['mrp.repair'].search(cr, uid, [
            ('fees_lines', 'in', ids)], context=context)

    _columns = {
        'amount_tax': fields.function(
            _amount_tax, string='Taxes',
            store={
              'mrp.repair': (
              lambda self, cr, uid, ids, c={}: ids,
              ['operations', 'fees_lines'], 10),
               'mrp.repair.line': (
                    _get_lines,
                          ['price_unit',
                           'price_subtotal',
                           'product_id',
                           'tax_id',
                           'product_uom_qty',
                           'product_uom'], 10),
               'mrp.repair.fee': (
                   _get_fee_lines,
                         ['price_unit',
                          'price_subtotal',
                          'product_id',
                          'tax_id',
                          'product_uom_qty',
                          'product_uom'], 10),
            }),
    }
