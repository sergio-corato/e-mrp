# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, fields, exceptions, _


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def do_merge(self, keep_references=True, date_invoice=False):
        invoices_info, invoice_lines_info = super(AccountInvoice, self).\
            do_merge(keep_references=keep_references, date_invoice=date_invoice
                     )
        mrp_obj = self.env['mrp.repair']
        for new_invoice_id in invoices_info:
            mrp_ids = mrp_obj.search(
                [('invoice_id', 'in', invoices_info[new_invoice_id])])
            mrp_ids.write({'invoice_id': new_invoice_id})
        return invoices_info, invoice_lines_info
