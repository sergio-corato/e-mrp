# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _
from dateutil.relativedelta import relativedelta


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    end_date_to_invoice = fields.Date()

    @api.multi
    def split_qty(self):
        for line in self.env['sale.order.line'].browse(
                self.order_line_ids.ids):
            if not self.end_date_to_invoice:
                return
            end_date = line.end_date
            date_invoice = fields.Date.from_string(self.end_date_to_invoice)
            number_of_days = line.compute_rental_days(
                self.end_date_to_invoice,
                line.start_date,
                line.product_id.categ_id,
            )
            line.write({
                'end_date': date_invoice,
                'number_of_days': number_of_days,
                'product_uom_qty': number_of_days,
            })
            date_invoice += relativedelta(days=1)
            start_date = fields.Date.to_string(date_invoice)
            number_of_days = line.compute_rental_days(
                end_date,
                start_date,
                line.product_id.categ_id,
            )
            new_line_id = line.copy(default={
                'end_date': end_date,
                'start_date': start_date,
                'number_of_days': number_of_days,
                'product_uom_qty': number_of_days,
                'rental_type': 'rental_extension',
                'extension_rental_id': line.sell_rental_id.id,
            })
            new_line_id.button_confirm()
        order_id = self.env['sale.order'].browse(
            self._context['active_id'])
        act_window = self.env['ir.actions.act_window']
        res = act_window.for_xml_id('sale', 'action_order_line_tree2')
        res['context'] = {
            'search_default_uninvoiced': 1,
            'search_default_order_id': order_id and
                order_id.id or False,
        }
        return res

    @api.model
    def _get_order_lines_rental(self):
        line_ids = self.env['sale.order'].browse(
            self._context['active_id']).order_line.filtered(
            lambda x: x.rental_type and not x.invoiced
        )
        return line_ids

    order_line_ids = fields.Many2many(
        comodel_name='sale.order.line',
        relation='rental_advance_sale_order_line_rel',
        column1='order_line_id', column2='advance_id',
        string='Order lines',
        default=_get_order_lines_rental,
        help='Select order lines to print details in invoice'
    )
