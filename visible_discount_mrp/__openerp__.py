{
    'name': 'Visible Discount in Mrp',
    'version': '8.0.1.0.0',
    'category': "Mrp",
    'summary': 'Enable discount visibility separately on Mrp Line from pricelist',
    'description': """
        Enable discount visibility separately on Mrp Line from pricelist""",
    'author': 'sc',
    'website': 'https://efatto.it',
    'depends': ['sale', 'mrp'],
    'data': [],
    'installable': True,
}
