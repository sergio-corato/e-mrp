# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, _, models, fields


class Machinery(models.Model):
    _inherit = "machinery"

    partner_id = fields.Many2one(
        'res.partner',
        string='Partner that own this machine')
    ownership = fields.Selection(
        selection_add=[('partner', 'Partner')],
        default='partner',)
    product = fields.Many2one(
        required=True)
    code = fields.Char()
    account_id = fields.Many2one(
        comodel_name='account.account',
        string='Income account used for invoice or account moves',
        company_dependent=True
    )
