# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, _, models, fields


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    scan_lot = fields.Char(string='Scan or write lot name')

    @api.onchange('scan_lot')
    def onchange_scan(self):
        if self.scan_lot:
            lot_id = self.env['stock.production.lot'].search([
                ('name', '=', self.scan_lot),
            ])
            if lot_id:
                move_ids = self.move_lines.filtered(
                    lambda x: x.product_id == lot_id.product_id and not
                    x.restrict_lot_id and not x.state == 'assigned')
                if move_ids:
                    move = move_ids[0]
                    new_move_id = move.split(move, 1.0)
                    if new_move_id:
                        new_move = self.env['stock.move'].browse(
                            new_move_id)
                        new_move.write({'restrict_lot_id': lot_id.id})
                        new_move.action_assign()
        self.scan_lot = ''
