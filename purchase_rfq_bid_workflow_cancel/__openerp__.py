# -*- coding: utf-8 -*-
# Copyright 2020 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    'name': 'Purchase RFQ-BID workflow restore cancel',
    'version': '8.0.1.0.0',
    'category': 'other',
    'author': 'Sergio Corato',
    'description': 'Purchase RFQ-BID workflow restore cancel.',
    'website': 'https://efatto.it',
    'license': 'LGPL-3',
    'depends': [
        'purchase_rfq_bid_workflow',
    ],
    'data': [
        'views/purchase_order.xml',
    ],
    'installable': True
}
