# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api
try:
    import googlemaps
except ImportError:
    googlemaps = False


class MrpRepair(models.Model):
    _inherit = 'mrp.repair'

    start_address = fields.Char(string='Start Address')
    end_address = fields.Char(string='End Address')
    distance = fields.Float(string='Driving Distance')
    destination = fields.Char(string='Found destination', readonly=True)
    origin = fields.Char(string='Found origin', readonly=True)

    @api.multi
    @api.depends('start_address', 'end_address')
    def compute_distance(self):
        # api_key = self.env['ir.config_parameter'].get_param(
        #     'google_maps_key')
        api_key = self.env.user.company_id.google_maps_key
        for repair in self:
            if repair.start_address and repair.end_address and repair.on_site \
                    and api_key:
                if googlemaps:
                    client = googlemaps.Client(api_key)
                    try:
                        matrix = client.distance_matrix(
                            repair.start_address, repair.end_address)
                        repair.destination = matrix['destination_addresses'][0]
                        repair.origin = matrix['origin_addresses'][0]
                        repair.distance = matrix['rows'][0]['elements'][0][
                            'distance']['value'] / 1000.00
                    except:
                        pass
