# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields


class ResCompany(models.Model):
    _inherit = 'res.company'
    google_maps_key = fields.Char()


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    google_maps_key = fields.Char(
        related='company_id.google_maps_key')
