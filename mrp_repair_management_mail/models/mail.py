# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, workflow, api


class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self):
        if self._context.get('default_model', False) == 'mrp.repair' and \
                self._context.get('default_res_id', False) and \
                self._context.get('mark_mr_as_sent', False):
            workflow.trg_validate(
                self._uid, 'mrp.repair', self._context['default_res_id'],
                'action_wait_confirmation', self._cr)
        return super(MailComposeMessage, self).send_mail()
