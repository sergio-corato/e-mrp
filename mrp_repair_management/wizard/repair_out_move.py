# -*- coding: utf-8 -*-
from openerp import models, fields, api, workflow


class RepairOutMove(models.TransientModel):
    _name = 'repair.out.move'
    _description = 'Create or update out moves of operations products'

    date = fields.Datetime(required=True, default=fields.Datetime.now())

    @api.multi
    def repair_out_move(self):
        repair_order = self.env['mrp.repair'].browse(self._context['active_id'])
        repair_order.update_out_moves(date=self.date)
        return {'type': 'ir.actions.act_window_close'}
