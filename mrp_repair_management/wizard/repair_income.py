# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, workflow


class RepairIncome(models.TransientModel):
    _name = 'repair.income'
    _description = 'Create income picking of repairable product'

    origin = fields.Char()
    date = fields.Datetime(required=True, default=fields.Datetime.now())

    @api.multi
    def confirm_income_picking(self):
        repair_order = self.env['mrp.repair'].browse(
            self._context['active_id'])
        workflow.trg_validate(
            self._uid, 'mrp.repair', repair_order.id,
            'reception_confirm', self._cr)
        if repair_order.in_picking_id:
            repair_order.in_picking_id.write({
                'origin': self.origin,
                'date': self.date,
                })
            repair_order.in_picking_id.action_confirm()
            repair_order.in_picking_id.force_assign()
            self.env['stock.move'].browse([
                x.id for x in repair_order.in_picking_id.move_lines if x.state != 'done'
            ]).action_done()
        return {'type': 'ir.actions.act_window_close'}
