# -*- coding: utf-8 -*-
# Copyright 2021 Sergio Corato <https://github.com/sergiocorato>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
from openerp import fields, workflow
from openerp.tests.common import TransactionCase
from dateutil.relativedelta import relativedelta


class TestMrpRepairManagement(TransactionCase):

    def setUp(self):
        super(TestMrpRepairManagement, self).setUp()
        self.src_location = self.env.ref('stock.stock_location_stock')
        self.dest_location = self.env.ref('stock.stock_location_customers')
        self.location_progress = self.env.ref(
            'mrp_repair_management.stock_location_repair_progress')
        self.scrap_location = self.env.ref('stock.stock_location_scrapped')
        self.partner = self.env.ref('base.res_partner_2')
        # Acoustic Bloc Screens, 16 on hand
        self.product1 = self.env.ref('product.product_product_25')
        # Cabinet with Doors, 8 on hand
        self.product2 = self.env.ref('product.product_product_10')
        self.machine_product = self.env['product.product'].create({
            'name': 'Machine product',
            'type': 'consu',
            'default_code': 'REPAIR',
            'machine_repair_ok': True,
        })
        self.partner_machine = self.env['machine.repair'].create({
            'name': 'Partner Machine',
            'product': self.machine_product.id,
            'status': 'active',
            'ownership': 'partner',
            'partner_id': self.partner.id,
        })
        self.own_machine = self.env['machine.repair'].create({
            'name': 'Own Machine',
            'product': self.machine_product.id,
            'status': 'active',
            'ownership': 'own',
            'partner_id': self.env.user.company_id.partner_id.id,
        })

    def _create_repair_order_line(self, repair, product, qty, type='add'):
        line = self.env['mrp.repair.line'].create({
            'name': 'Add product',
            'repair_id': repair.id,
            'type': type,
            'product_id': product.id,
            'product_uom_qty': qty,
            'product_uom': product.uom_id.id,
            'price_unit': 5,
            'location_id': self.env.ref('stock.stock_location_stock').id,
            'location_dest_id': self.env.ref('stock.location_production').id,
        })
        line.onchange_type()
        line._convert_to_write(line._cache)
        return line

    def _receive_machine_wizard(self, repair_id):
        return self.env['repair.income'].with_context(
            active_id=repair_id
            ).create({})

    def _repair_out_move_wizard(self, repair_id, move_date):
        return self.env['repair.out.move'].with_context(
            active_id=repair_id
            ).create({'date': move_date})

    def _repair_partner_machine(
            self, onsite=False, machine=False, update_out_moves=False):
        repair = self.env['mrp.repair'].create({
            'name': 'Onsite' if onsite else 'On factory',
            'product_id': self.product1.id,
            'product_uom': self.product1.uom_id.id,
            'partner_id': machine.partner_id.id,
            'machine_id': machine.id,
            'note': 'Reason of repair request',
            'product_qty': 1,
            'location_id': self.env.ref('stock.stock_location_stock').id,
            'location_dest_id': self.env.ref('stock.stock_location_stock').id,
            'on_site': onsite,
            })
        repair.onchange_partner_id()
        repair.write(repair._convert_to_write(repair._cache))
        repair.onchange_machine_id()
        repair.write(repair._convert_to_write(repair._cache))
        repair.onchange_product_id()
        repair.write(repair._convert_to_write(repair._cache))
        self._create_repair_order_line(repair, self.product1, 5)
        self._create_repair_order_line(repair, self.product2, 3, 'remove')

        receive_machine_wiz = self._receive_machine_wizard(repair.id)
        receive_machine_wiz.confirm_income_picking()
        self.assertEqual(repair.state, "confirmed",
                         'Repair order should be in "Confirmed" state.')
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'start_analysis', repair._cr)
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'action_analyzing_end', repair._cr)
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'action_wait_confirmation', repair._cr)
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'action_confirmation', repair._cr)
        repair.action_repair_start()
        self.assertEqual(repair.state, "under_repair",
                         'Repair order should be in "Under repair" state.')
        if update_out_moves:
            date_move = fields.Date.from_string(fields.Date.today())
            date_move += relativedelta(days=-180)
            move_date = fields.Date.to_string(date_move)
            update_out_moves_wiz = self._repair_out_move_wizard(repair.id, move_date)
            update_out_moves_wiz.repair_out_move()
            for op in repair.operations.filtered(lambda x: x.move_id):
                self.assertEqual(
                    op.move_id.date[:10], move_date,
                    'Date of move would be that imputed on wizard.')
                if op.product_id == self.product1:
                    self.assertEqual(
                        op.move_id.location_dest_id,
                        self.location_progress,
                        'Consumed product would be in location progress')
                else:
                    self.assertEqual(
                        op.move_id.location_dest_id,
                        self.scrap_location,
                        'Consumed product would be in scrapped location')
                    self.assertEqual(
                        op.move_id.location_id,
                        self.dest_location,
                        'Scrapped product would come from location dest'
                    )
        repair.action_repaired()
        state = 'wait_delivery'
        if onsite:
            state = 'wait_outgo'
        self.assertEqual(repair.state, state,
                         'Repair order should be in "Deliver to Customer" state.')
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'action_repair_done', repair._cr)
        repair.state = 'wait_outgo'
        self.assertEqual(repair.state, "wait_outgo",
                         'Repair order should be in "Register outgo" state.')
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'action_outgo', repair._cr)
        repair.action_repair_end()
        self.assertEqual(repair.state, "2binvoiced",
                         'Repair order should be in "To be invoiced" state.')
        repair.action_invoice_create()
        self.assertTrue(repair.invoice_id, 'Invoice has not been created.')
        repair.action_finish()
        self.assertEqual(repair.state, "done",
                         'Repair order should be in "Done" state.')
        self.assertTrue(repair.operations)
        moves = repair.operations.filtered(lambda x: x.move_id)
        if update_out_moves:
            moves = repair.out_products_picking_id.move_lines
        for move in moves:
            if move.product_id == self.product1:
                self.assertEqual(
                    move.location_dest_id,
                    self.dest_location,
                    'Consumed product would be in location dest')
            else:
                self.assertEqual(
                    move.location_dest_id,
                    self.dest_location if update_out_moves else self.scrap_location,
                    'Consumed product would be in %s location' %
                    self.dest_location.name if update_out_moves
                    else self.scrap_location.name
                )
                self.assertEqual(
                    move.location_id,
                    self.scrap_location if update_out_moves else self.dest_location,
                    'Scrapped product would come from location %s' %
                    self.scrap_location.name if update_out_moves
                    else self.dest_location.name
                )

    def test_not_onsite_repair_partner_machine(self):
        self._repair_partner_machine(machine=self.partner_machine)

    def test_onsite_repair_partner_machine(self):
        self._repair_partner_machine(onsite=True, machine=self.partner_machine)

    def test_not_onsite_repair_partner_machine_out_moves(self):
        self._repair_partner_machine(
            machine=self.partner_machine, update_out_moves=True)

    def test_onsite_repair_partner_machine_out_moves(self):
        self._repair_partner_machine(
            onsite=True, machine=self.partner_machine, update_out_moves=True)

    def _repair_own_machine(self, onsite=False, machine=False, update_out_moves=True):
        repair = self.env['mrp.repair'].create({
            'name': 'Onsite' if onsite else 'On factory',
            'product_id': self.product1.id,
            'product_uom': self.product1.uom_id.id,
            'partner_id': machine.partner_id.id,
            'machine_id': machine.id,
            'note': 'Reason of repair request',
            'product_qty': 1,
            'location_id': self.env.ref('stock.stock_location_stock').id,
            'location_dest_id': self.env.ref('stock.stock_location_stock').id,
            'on_site': onsite,
            })
        repair.onchange_partner_id()
        repair.write(repair._convert_to_write(repair._cache))
        repair.onchange_machine_id()
        repair.write(repair._convert_to_write(repair._cache))
        repair.onchange_product_id()
        repair.write(repair._convert_to_write(repair._cache))
        self._create_repair_order_line(repair, self.product1, 5)
        self._create_repair_order_line(repair, self.product2, 3, 'remove')

        repair.receive_machine()
        repair.action_repair_start()
        self.assertEqual(repair.state, "under_repair",
                         'Repair order should be in "Under repair" state.')
        if update_out_moves:
            date_move = fields.Date.from_string(fields.Date.today())
            date_move += relativedelta(days=-180)
            move_date = fields.Date.to_string(date_move)
            update_out_moves_wiz = self._repair_out_move_wizard(repair.id, move_date)
            update_out_moves_wiz.repair_out_move()
            for op in repair.operations.filtered(lambda x: x.move_id):
                self.assertEqual(
                    op.move_id.date[:10], move_date,
                    'Date of move would be that imputed on wizard.')
                if op.product_id == self.product1:
                    self.assertEqual(
                        op.move_id.location_dest_id,
                        self.location_progress,
                        'Consumed product would be in location progress')
                else:
                    self.assertEqual(
                        op.move_id.location_dest_id,
                        self.scrap_location,
                        'Consumed product would be in scrapped location')
                    self.assertEqual(
                        op.move_id.location_id,
                        self.dest_location,
                        'Scrapped product would come from location dest'
                    )
        repair.action_repaired()
        state = 'wait_delivery'
        if onsite:
            state = 'wait_outgo'
        self.assertEqual(repair.state, state,
                         'Repair order should be in "Deliver to Customer" state.')
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'action_repair_done', repair._cr)
        repair.state = 'wait_outgo'
        self.assertEqual(repair.state, "wait_outgo",
                         'Repair order should be in "Register outgo" state.')
        workflow.trg_validate(
            repair._uid, 'mrp.repair', repair.id,
            'action_outgo', repair._cr)
        repair.action_repair_end()
        repair.action_finish()
        self.assertEqual(repair.state, "done",
                         'Repair order should be in "Done" state.')
        self.assertTrue(repair.operations)
        moves = repair.operations.filtered(lambda x: x.move_id)
        if update_out_moves:
            moves = repair.out_products_picking_id.move_lines
        for move in moves:
            if move.product_id == self.product1:
                self.assertEqual(
                    move.location_dest_id,
                    self.dest_location,
                    'Consumed product would be in location dest')
            else:
                self.assertEqual(
                    move.location_dest_id,
                    self.dest_location if update_out_moves else self.scrap_location,
                    'Consumed product would be in %s location' %
                    self.dest_location.name if update_out_moves
                    else self.scrap_location.name
                )
                self.assertEqual(
                    move.location_id,
                    self.scrap_location if update_out_moves else self.dest_location,
                    'Scrapped product would come from location %s' %
                    self.scrap_location.name if update_out_moves
                    else self.dest_location.name
                )

    def test_not_onsite_repair_own_machine(self):
        self._repair_own_machine(machine=self.own_machine)

    def test_onsite_repair_own_machine(self):
        self._repair_own_machine(onsite=True, machine=self.own_machine)

    def test_not_onsite_repair_own_machine_update_moves(self):
        self._repair_own_machine(machine=self.own_machine, update_out_moves=True)

    def test_onsite_repair_own_machine_update_moves(self):
        self._repair_own_machine(
            onsite=True, machine=self.own_machine, update_out_moves=True)
