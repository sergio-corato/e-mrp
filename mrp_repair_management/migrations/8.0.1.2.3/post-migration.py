# -*- coding: utf-8 -*-
# Copyright 2021 Sergio Corato <https://github.com/sergiocorato>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from openupgradelib import openupgrade
from openerp import SUPERUSER_ID
from openerp.api import Environment

import logging
_logger = logging.getLogger(__name__)


@openupgrade.migrate()
def migrate(cr, version):
    env = Environment(cr, SUPERUSER_ID, {})
    # 1. aggiornare tutte le riparazioni ancora aperte per quanto riguarda i movimenti
    # sul wip
    repairs = env['mrp.repair'].search([
        ('state', 'not in', ['draft', 'done', 'cancel']),
        ('out_products_picking_id', '!=', False),
    ])
    for repair in repairs:
        # Ritorniamo o annulliamo il movimento di uscita dal wip.
        # Verrà ricreato alla fine della riparazione nel normale flusso.
        if repair.out_products_picking_id.state in ['draft', 'done']:
            _logger.info('Return picking of repair %s' % repair.name)
            repair.return_picking(repair.out_products_picking_id)
            repair.write({'out_products_picking_id': False})
        else:
            _logger.info('Out picking of repair %s is not done: trying to '
                         'force done' % repair.name)
            repair.out_products_picking_id.action_confirm()
            repair.out_products_picking_id.force_assign()
            _logger.info('Forced out picking %s of repair %s' % (
                repair.out_products_picking_id.name,
                repair.name,
            ))
            _logger.info('Return forced picking of repair %s in state %s' % (
                repair.name, repair.state))
            try:
                repair.return_picking(repair.out_products_picking_id)
            except:
                _logger.info('Picking %s return failed' %
                             repair.out_products_picking_id.name)
                pass
            repair.write({'out_products_picking_id': False})
        # Aggiorniamo le uscite dal wip in data di creazione della riga
        if repair.operations:
            # nella versione precedente la migrazione
            # le uscite dei materiali sono create solo se la ripazione è completata
            _logger.info('Update product out moves of repair %s' % repair.name)
            repair.with_context(bypass_state_update=True).update_out_moves()

    # 2. aggiornare le riparazioni chiuse nel 2021 aperte prima del 2021
    # in data di creazione della riga del materiale -> impostato di default se non
    # richiesta in data specifica
    done_repairs = env['mrp.repair'].search([
        ('state', '=', 'done'),
        ('date', '<', '2021-01-01'),
        ('out_sppp_id.date', '>', '2020-12-31'),
    ])
    for done_repair in done_repairs:
        # i movimenti nel wip sono già chiusi ma in date errate, li rifacciamo in date
        # corrette
        # In teoria hanno sempre il movimento di uscita dal wip, lo ritorniamo o
        # annulliamo, e lo ricreiamo in data di consegna del ddt,
        # visto che la riparazione è già completata.
        done_repair_state = done_repair.state
        if done_repair.out_products_picking_id:
            if done_repair.out_products_picking_id.state in ['draft', 'done']:
                _logger.info('Return picking of done repair %s' % done_repair.name)
                done_repair.return_picking(done_repair.out_products_picking_id)
                done_repair.write({'out_products_picking_id': False})
            else:
                _logger.info('Out picking of done repair %s is not done: trying to '
                             'cancel' % done_repair.name)
                res = done_repair.out_products_picking_id.action_cancel()
                _logger.info('Out picking cancel result %s' % res)
        else:
            _logger.info('Done repair %s ignored as missing out_products_picking_id' %
                         done_repair.name)
            continue
        if done_repair.operations:
            _logger.info(
                'Update product out moves of done repair %s' % done_repair.name)
            # le uscite dei materiali sono già state create nella versione precedente
            # la migrazione, e sono da wip a clienti, per cui vanno aggiornate per avere
            # la situazione corretta del wip
            for operation in done_repair.operations:
                operation.write({
                    'move_id': False,
                })
            done_repair.with_context(bypass_state_update=True).update_out_moves()
        # quindi facciamo rifare l'out finale dal wip al cliente, che va già in automa-
        # tico in data del ddt
        out_picking = done_repair.out_products_picking_create()
        _logger.info('Recreated out products picking %s for repair %s' % (
            out_picking.name, done_repair.name,
        ))
        if done_repair_state != done_repair.state:
            _logger.info('State of done repair is changed from %s to %s!' % (
                done_repair_state, done_repair.state
            ))
