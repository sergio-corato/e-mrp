# -*- coding: utf-8 -*-
# Copyright 2021 Sergio Corato <https://github.com/sergiocorato>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from openupgradelib import openupgrade
from openerp import SUPERUSER_ID
from openerp.api import Environment

import logging
_logger = logging.getLogger(__name__)


@openupgrade.migrate()
def migrate(cr, version):
    env = Environment(cr, SUPERUSER_ID, {})
    # 2. correggere le riparazioni chiuse nel 2021 aperte prima del 2021
    # creando dei move duplicati da out_products_picking_id.move_lines
    # con ubicazione destinazione WH/STOCK
    done_repairs = env['mrp.repair'].search([
        ('state', '=', 'done'),
        ('date', '<', '2021-01-01'),
        ('out_sppp_id.date', '>', '2020-12-31'),
        ('out_products_picking_id', '!=', False)
    ])
    for done_repair in done_repairs:
        for move in done_repair.out_products_picking_id.move_lines:
            new_move = move.copy(
                default={
                    'location_dest_id': env.ref('stock.stock_location_stock').id
                })
            new_move.with_context(
                date_mrp=done_repair.out_products_picking_id.date).action_done()
        _logger.info('Duplicated moves of done repair %s on date %s' % (
                done_repair.name,
                done_repair.out_products_picking_id.date
            )
        )
