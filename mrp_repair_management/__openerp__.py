# -*- coding: utf-8 -*-
#
#
#    Copyright (C) 2017 Sergio Corato
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
{
    'name': 'Mrp repair management',
    'version': '8.0.1.2.4',
    'category': 'other',
    'author': 'Sergio Corato',
    'website': 'http://www.efatto.it',
    'license': 'AGPL-3',
    'description': 'Add workflow for more complete repair management',
    'depends': [
        'hr_timesheet',
        'mrp_repair',
        'mrp_repair_analytic',
        'mrp_repair_discount',
        'mrp_repair_full_editable',
        'mrp_repair_machine',
        'stock_ddt_data',
        'stock_move_backdating',
        'stock_picking_package_preparation',
        'stock_sale_order_line',
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/repair_income.xml',
        'wizard/repair_out_move.xml',
        'data/analytic.xml',
        'data/location.xml',
        'data/stock_picking_type.xml',
        'data/sequence.xml',
        'views/css.xml',
        'views/mrp_repair_workflow.xml',
        'views/mrp_repair.xml',
        'views/stock_move.xml',
        'views/mrp_repair_report.xml',
    ],
    'installable': True
}
