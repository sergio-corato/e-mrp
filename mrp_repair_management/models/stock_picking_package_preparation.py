# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, models, fields


class StockPickingPackagePreparation(models.Model):
    _inherit = 'stock.picking.package.preparation'

    @api.multi
    def get_mrp_repair(self):
        for ddt in self:
            repair = self.env['mrp.repair'].search([
                ('out_sppp_id', '=', ddt.id)
            ])
            if repair:
                return repair
        return False

    #  todo if unlink stock picking, change state mrp repair 2binvoiced and
    # remove out_sppp_id
