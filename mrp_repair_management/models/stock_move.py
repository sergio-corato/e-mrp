# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import fields, models, exceptions, api, _


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def _get_price_unit_net(self):
        for line in self:
            if line.sale_line_id:
                line.sale_price_unit_net = line.sale_line_id.price_unit * (
                    1 - line.sale_line_id.discount / 100.0)
            elif line.mrp_repair_line_id:
                line.sale_price_unit_net = line.mrp_repair_line_id.\
                    price_unit * (
                        1 - line.mrp_repair_line_id.discount / 100.0)

    mrp_repair_line_id = fields.Many2one(
        'mrp.repair.line', copy=False)
    sale_price_unit_net = fields.Float(
        compute=_get_price_unit_net,
        string='Sale Price Net',
        help='Sale price unit net'
    )

    @api.multi
    def action_done(self):
        result = super(StockMove, self).action_done()
        # overwrite date field where applicable
        if 'date_mrp' in self.env.context:
            date_mrp = self.env.context.get('date_mrp')
            for move in self:
                move.date = date_mrp
                if move.quant_ids:
                    move.quant_ids.sudo().write({'in_date': date_mrp})
        return result
