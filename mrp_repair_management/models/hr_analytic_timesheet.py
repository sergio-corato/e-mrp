# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, _, models, fields, workflow
import openerp.addons.decimal_precision as dp


class HrAnalyticTimesheet(models.Model):
    _inherit = 'hr.analytic.timesheet'

    mrp_repair_id = fields.Many2one(
        comodel_name='mrp.repair')
    product_id = fields.Many2one(
        comodel_name='product.product')
    amount_sale = fields.Float(
        precision=dp.get_precision('Account'),
        compute='_get_amount_sale')

    @api.multi
    @api.depends('unit_amount', 'product_id')
    def _get_amount_sale(self):
        for ts in self:
            partner_id = ts.mrp_repair_id.partner_id
            pricelist_id = ts.mrp_repair_id.pricelist_id
            # todo get from repair, but it is not get from partner!
            product_id = ts.product_id

            if pricelist_id and product_id:
                price = pricelist_id.with_context({
                    'uom': product_id.uom_id.id,
                    'date': fields.Date.today(),
                }).price_get(
                    product_id.id, ts.unit_amount,
                    partner_id.id)[pricelist_id.id]
            else:
                price = ts.product_id.lst_price
            ts.amount_sale = ts.unit_amount * price

    @staticmethod
    def _create_repair_line(repair, total_timesheet, product, name, taxes_ids):
        partner_id = repair.partner_id
        pricelist_id = repair.pricelist_id
        # todo get from repair, but it is not get from partner!
        if pricelist_id and product:
            price = pricelist_id.with_context({
                'uom': product.uom_id.id,
                'date': fields.Date.today(),
            }).price_get(
                product.id, total_timesheet,
                partner_id.id)[pricelist_id.id]
        else:
            price = product.lst_price
        return {
            'name': name,
            'type': 'add',
            'repair_id': repair.id,
            'product_id': product.id,
            'product_uom_qty': total_timesheet,
            'product_uom': product.uom_id.id,
            'price_unit': price,
            'location_id': repair.location_id.id,
            'location_dest_id': repair.location_dest_id.id,
            'is_timesheet': True,
            'to_invoice': not repair.on_warranty,
            'load_cost': False,
            'tax_id': [(6, 0, taxes_ids.ids)],
        }

    @api.model
    def create(self, vals):
        res = super(HrAnalyticTimesheet, self).create(vals)
        mrp_repair_id = vals.get('mrp_repair_id', False)
        if mrp_repair_id:
            repair = self.env['mrp.repair'].browse(mrp_repair_id)
            timesheet_line = repair.operations.filtered('is_timesheet')
            if timesheet_line:
                timesheet_line.unlink()
            service_line = repair.operations.filtered(
                lambda x: x.product_id.type == 'service')
            if service_line:
                service_line.unlink()
            products = repair.hr_analytic_timesheet_ids.mapped('product_id')
            for product in products:
                #  create 1 line grouped for service
                total_unit_timesheet = sum(
                    x.unit_amount for x in
                    repair.hr_analytic_timesheet_ids.filtered(
                        lambda x: x.product_id == product))
                name = ', '.join(x.name for x in
                                 repair.hr_analytic_timesheet_ids.filtered(
                                     lambda x: x.product_id == product))
                fpos = repair.fiscal_position if repair.fiscal_position else \
                    repair.partner_id.property_account_position
                if fpos:
                    taxes_ids = fpos.map_tax(product.taxes_id)
                else:
                    taxes_ids = product.taxes_id.filtered(
                        lambda x: x.company_id == repair.company_id)
                repair_line_vals = self._create_repair_line(
                    repair, total_unit_timesheet, product, name, taxes_ids)
                self.env['mrp.repair.line'].create(repair_line_vals)
        return res

    @api.multi
    def write(self, vals):
        res = super(HrAnalyticTimesheet, self).write(vals)
        if self.mrp_repair_id:
            timesheet_line = self.mrp_repair_id.operations.filtered(
                'is_timesheet')
            if timesheet_line:
                timesheet_line.unlink()
            repair = self.mrp_repair_id
            products = repair.hr_analytic_timesheet_ids.mapped(
                'product_id')
            for product in products:
                total_unit_timesheet = sum(
                    x.unit_amount for x in
                    repair.hr_analytic_timesheet_ids.filtered(
                        lambda x: x.product_id == product))
                name = ', '.join(x.name for x in
                                 repair.hr_analytic_timesheet_ids.filtered(
                                     lambda x: x.product_id == product))
                fpos = repair.fiscal_position if repair.fiscal_position else \
                    repair.partner_id.property_account_position
                if fpos:
                    taxes_ids = fpos.map_tax(product.taxes_id)
                else:
                    taxes_ids = product.taxes_id.filtered(
                        lambda x: x.company_id == repair.company_id)
                repair_line_vals = self._create_repair_line(
                    repair, total_unit_timesheet, product, name, taxes_ids)
                self.env['mrp.repair.line'].create(repair_line_vals)
        return res
