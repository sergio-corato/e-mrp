# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato <https://github.com/sergiocorato>

from openerp import tools
from openerp import api, models, fields


class MrpRepairReport(models.Model):
    _name = "mrp.repair.report"
    _description = "Mrp Repair Statistics"
    _auto = False
    _rec_name = 'date'

    date = fields.Datetime('Date Order', readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    product_uom = fields.Many2one(
        'product.uom', 'Unit of Measure', readonly=True)
    product_uom_qty = fields.Float('# of Qty', readonly=True)
    partner_id = fields.Many2one('res.partner', 'Partner', readonly=True)
    partner_invoice_id = fields.Many2one(
        'res.partner', 'Partner Invoice', readonly=True)
    company_id = fields.Many2one('res.company', 'Company', readonly=True)
    user_id = fields.Many2one('res.users', 'Salesperson', readonly=True)
    price_total = fields.Float('Total Price', readonly=True)
    to_invoice = fields.Boolean('To invoice', readonly=True)
    on_site = fields.Boolean('On site', readonly=True)
    on_warranty = fields.Boolean('On warranty', readonly=True)
    categ_id = fields.Many2one(
        'product.category', 'Category of Product', readonly=True)
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('cancel', 'Cancelled'),
        ('confirmed', 'Confirmed'),
        ('under_repair', 'Under Repair'),
        ('ready', 'Ready to Repair'),
        ('2binvoiced', 'To be Invoiced'),
        ('invoice_except', 'Invoice Exception'),
        ('done', 'Repaired'),
        ('under_analysis', 'Under Analysis'),
        ('analyzed', 'Analyzed'),
        ('wait_confirmation', 'Waiting for confirmation from Customer'),
        ('wait_delivery', 'Waiting to deliver to Customer'),
        ('wait_outgo', 'Waiting for Outgo'),
    ], 'Mrp Status', readonly=True)
    pricelist_id = fields.Many2one(
        'product.pricelist', 'Pricelist', readonly=True)
    analytic_account = fields.Many2one(
        'account.analytic.account', 'Analytic Account', readonly=True)

    _order = 'date desc'

    def _select(self):
        select_str = """
            WITH currency_rate (currency_id, rate, date_start, date_end) AS (
                SELECT r.currency_id, r.rate, r.name AS date_start,
                    (SELECT name FROM res_currency_rate r2
                    WHERE r2.name > r.name AND
                        r2.currency_id = r.currency_id
                     ORDER BY r2.name ASC
                     LIMIT 1) AS date_end
                FROM res_currency_rate r
            )
            SELECT min(l.id) as id,
                l.product_id as product_id,
                t.uom_id as product_uom,
                l.to_invoice as to_invoice,
                r.on_site as on_site,
                r.on_warranty as on_warranty,
                sum(l.product_uom_qty / u.factor * u2.factor) 
                    as product_uom_qty,
                sum(l.product_uom_qty * l.price_unit / cr.rate * (
                    100.0 - coalesce(l.discount, 0.0)) / 100.0) as price_total,
                count(*) as nbr,
                r.date as date,
                r.partner_id as partner_id,
                r.partner_invoice_id as partner_invoice_id,
                r.user_id as user_id,
                r.company_id as company_id,
                r.state,
                t.categ_id as categ_id,
                r.pricelist_id as pricelist_id,
                r.analytic_account as analytic_account
        """
        return select_str

    def _from(self):
        from_str = """
                mrp_repair_line l
                    join mrp_repair r on (l.repair_id=r.id)
                        left join product_product p on (l.product_id=p.id)
                            left join product_template t on (
                                p.product_tmpl_id=t.id)
                    left join product_uom u on (u.id=l.product_uom)
                    left join product_uom u2 on (u2.id=t.uom_id)
                    left join product_pricelist pp on (r.pricelist_id = pp.id)
                    left join currency_rate cr on (
                        cr.currency_id = pp.currency_id and
                        cr.date_start <= coalesce(r.date, now()) and (
                            cr.date_end is null 
                            or cr.date_end > coalesce(r.date, now())))
        """
        return from_str

    def _group_by(self):
        group_by_str = """
            GROUP BY l.product_id,
                    l.repair_id,
                    l.to_invoice,
                    r.on_site,
                    r.on_warranty,
                    p.product_tmpl_id,
                    l.product_uom,
                    t.uom_id,
                    t.categ_id,
                    r.date,
                    r.partner_id,
                    r.partner_invoice_id,
                    r.user_id,
                    r.company_id,
                    r.state,
                    r.pricelist_id,
                    r.analytic_account
        """
        return group_by_str

    def init(self, cr):
        # self._table = mrp_repair_report
        tools.drop_view_if_exists(cr, self._table)
        cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))