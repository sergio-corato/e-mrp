# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, _, models, fields, workflow
from openerp.exceptions import Warning, ValidationError
from openerp.tools.safe_eval import safe_eval


class MrpRepairLine(models.Model):
    _inherit = 'mrp.repair.line'

    type = fields.Selection(default='add')
    name = fields.Text('Description', required=True)
    is_timesheet = fields.Boolean()
    product_id = fields.Many2one(domain=[('sale_ok', '=', True)])

    @api.onchange('type')
    def onchange_type(self):
        location_obj = self.env['stock.location']
        # if add product:
        if self.type == 'add':
            to_invoice = not self.repair_id.on_warranty
            # from location is stock
            location_id = self.repair_id.location_dest_id
            # if to invoice, from stock go to partner
            if to_invoice:
                location_dest_id = self.repair_id.address_id.\
                    property_stock_customer
            # else from stock go to production virtual loc
            else:
                location_prod_id = location_obj.search(
                    [('usage', '=', 'production')])
                location_dest_id = location_prod_id and location_prod_id[
                    0] or False
        # remove product: from partner go to scrap
        elif self.type == 'remove':
            scrap_location_ids = location_obj.search([
                ('scrap_location', '=', True)])
            to_invoice = False
            location_dest_id = scrap_location_ids and \
                scrap_location_ids[0] or False
            location_id = self.repair_id.address_id.property_stock_customer
        #no type: all to false
        else:
            location_id = location_dest_id = to_invoice = False

        self.to_invoice = to_invoice
        self.location_id = location_id
        self.location_dest_id = location_dest_id

    @api.multi
    def unlink(self):
        for line in self:
            if line.move_id and line.move_id.state != 'cancel':
                raise ValidationError(_(
                    'You cannot delete repair line with move in state different from '
                    'cancel! Create a new one of inverse type to adjust quantity.'))
        return super(MrpRepairLine, self).unlink()

    @api.one
    @api.constrains('product_uom_qty', 'move_id', 'product_id')
    def _check_move_product_uom_qty(self):
        if self.move_id and (
                self.move_id.product_uom_qty != (
                    self.product_uom_qty * (-1 if self.type == 'remove' else 1)
                ) or self.product_id != self.move_id.product_id
        ):
            raise ValidationError(_(
                'You cannot modify quantity or product of repair line already '
                'transferred! Create a new one of inverse type to adjust quantity.'))


class MrpRepair(models.Model):
    _inherit = 'mrp.repair'
    _order = 'date desc'

    in_picking_id = fields.Many2one(
        comodel_name='stock.picking', copy=False,
        string='Income Picking of machine to repair')
    out_picking_id = fields.Many2one(
        comodel_name='stock.picking', copy=False,
        string='Outgo Picking of machine to repair')
    out_sppp_id = fields.Many2one(
        comodel_name='stock.picking.package.preparation', copy=False,
        string='Outgo SPPP of machine repaired')
    out_products_picking_id = fields.Many2one(
        comodel_name='stock.picking', copy=False,
        string='Outgo Picking of products')
    invoice_method = fields.Selection(
        default='after_repair')
    is_analyzed = fields.Boolean(copy=False)
    on_warranty = fields.Boolean()
    on_site = fields.Boolean()
    repairable = fields.Boolean(default=True)
    note = fields.Text(required=True, default=' ')
    state = fields.Selection(
        selection_add=[
            ('under_analysis', 'Under Analysis'),
            ('analyzed', 'Analyzed'),
            ('wait_confirmation', 'Waiting for confirmation from Customer'),
            ('wait_delivery', 'Waiting to deliver to Customer'),
            ('wait_outgo', 'Waiting for Outgo'),
        ], copy=False)
    date = fields.Datetime(default=fields.Datetime.now, required=True)
    client_order_ref = fields.Char()
    payment_term = fields.Many2one(
        comodel_name='account.payment.term')
    fiscal_position = fields.Many2one(
        comodel_name='account.fiscal.position')
    pricelist_id = fields.Many2one(
        comodel_name='product.pricelist')
    currency_id = fields.Many2one(
        related='pricelist_id.currency_id')
    user_id = fields.Many2one(
        comodel_name='res.users',
        default=lambda self: self.env.user,)
    mrp_repair_sent_ids = fields.One2many(
        comodel_name='mrp.repair', copy=False,
        string='Repair Quotations Sent',
        inverse_name='mrp_repair_parent_id')
    mrp_repair_parent_id = fields.Many2one(
        comodel_name='mrp.repair', copy=False,
        string='Parent Repair Order')
    revision_nr = fields.Integer(copy=False)
    original_name = fields.Char(copy=False)
    mrp_repair_confirmed_id = fields.Many2one(
        comodel_name='mrp.repair', copy=False,
        string='Repair Order Confirmed')
    machine_id = fields.Many2one(
        comodel_name='machine.repair',
        string='Machine to repair')
    machine_note = fields.Char(
        related='machine_id.note', readonly=True)
    frame = fields.Char(
        related='machine_id.frame', readonly=True)
    hr_analytic_timesheet_ids = fields.One2many(
        comodel_name='hr.analytic.timesheet', copy=False,
        inverse_name='mrp_repair_id',
        ondelete='set null')
    name = fields.Char(default='/')
    phone = fields.Char(
        related='partner_id.phone', readonly=True)
    email = fields.Char(
        related='partner_id.email', readonly=True)
    analytic_account = fields.Many2one(
        default=lambda self: self.env.ref(
            'mrp_repair_management.account_analytic_repair'))
    exclude_company_address_id = fields.Many2one(
        comodel_name='res.partner',
        compute='_get_exclude_company_address')
    cost_in_report = fields.Boolean()

    def _get_exclude_company_address(self):
        for repair in self:
            address_id = repair.address_id
            if repair.partner_id == self.env.user.company_id.partner_id:
                address_id = self.env.user.company_id.partner_id
            repair.exclude_company_address_id = address_id

    @api.multi
    def print_quotation(self):
        return self.env['report'].get_action(
            self, 'mrp_repair.report_mrp_repairorder')

    @api.multi
    def print_quotation_confirmed(self):
        return self.env['report'].get_action(
            self.mrp_repair_confirmed_id,
            'mrp_repair.report_mrp_repairorder')

    @api.multi
    def print_final(self):
        return self.env['report'].get_action(
            self, 'mrp_repair.report_mrp_repairorder')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            if vals.get('on_site', False):
                raise Warning(_(
                    'Missing name! Write on site repair unique name.'))
            else:
                vals['name'] = self.env['ir.sequence'].with_context(
                    fiscalyear_id=self.env['account.fiscalyear'].find(
                        dt=self.date or fields.Date.context_today(self)
                    )).get('mrp.repair') or '/'
        return super(MrpRepair, self).create(vals)

    # fix mrp_repair_full_editable fix
    @api.multi
    def onchange_product_id(self, product_id=None):
        res = super(MrpRepair, self).onchange_product_id(product_id)
        if res['value'].get('pricelist_id', False):
            res['value'].pop('pricelist_id')
        return res

    @api.onchange('machine_id')
    def onchange_machine_id(self):
        if self.machine_id:
            if self.machine_id.ownership == 'own':
                self.invoice_method = 'none'
            else:
                self.invoice_method = 'after_repair'
            self.product_id = self.machine_id.product

    @api.onchange('partner_id')
    @api.depends('address_id')
    def onchange_partner_id(self):
        pricelist_obj = self.env['product.pricelist']
        if not self.partner_id:
            self.address_id = self.partner_invoice_id = False,
            self.pricelist_id = pricelist_obj.browse(
                [('type', '=', 'sale')])[0]
        else:
            addr = self.partner_id.address_get(['delivery', 'invoice', 'default'])
            pricelist = self.partner_id.property_product_pricelist and \
                self.partner_id.property_product_pricelist.id or False
            self.address_id = addr['delivery'] or addr['default']
            self.partner_invoice_id = addr['invoice']
            self.pricelist_id = pricelist
            self.payment_term = self.partner_invoice_id.property_payment_term.\
                id if self.partner_invoice_id.property_payment_term else False

    @api.onchange('partner_invoice_id')
    def onchange_partner_invoice_id(self):
        self.payment_term = self.partner_invoice_id.property_payment_term.\
            id if self.partner_invoice_id.property_payment_term else False

    @api.onchange('on_warranty')
    def onchange_on_warranty(self):
        for op in self.operations:
            op.onchange_type()

    @api.multi
    def receive_machine(self):
        for repair_order in self:
            # if on_site or own machine do not receive machine product
            if repair_order.machine_id.ownership == 'own' or \
                    repair_order.on_site:
                workflow.trg_validate(
                    self._uid, 'mrp.repair', repair_order.id,
                    'direct_repair', self._cr)
                repair_order.write({'is_analyzed': True})
            # else receive machine product
            else:
                if not repair_order.in_picking_id:
                    picking_id = repair_order.income_picking_create()
                    if picking_id:
                        repair_order.write({'in_picking_id': picking_id.id})
                        return {
                            'name': _('Confirm Picking Receipt'),
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'repair.income',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                        }
                else:
                    workflow.trg_validate(
                        self._uid, 'mrp.repair', repair_order.id,
                        'reception_confirm', self._cr)
            return {}

    @api.multi
    def action_confirm(self):
        for repair in self:
            if repair.state == 'wait_confirmation' and repair.is_analyzed \
                    and not repair.on_warranty:
                repair.save_mrp_repair_confirmed()
            if repair.is_analyzed and repair.invoice_method == 'b4repair' and \
                    not repair.invoiced:
                repair.write({'state': '2binvoiced'})
                repair.save_mrp_repair_confirmed()
            elif not repair.on_warranty:  # because on warranty is already ok
                repair.write({'state': 'confirmed'})
        return {}

    @api.multi
    def save_mrp_repair_confirmed(self):
        if not self.mrp_repair_confirmed_id:
            defaults = {
                'state': 'cancel',
                'date': fields.Datetime.now(),
                'name': '%s/%s' % (self.name, 'CONF'),
            }
            confirmed_id = self.copy(defaults)
            self.write({'mrp_repair_confirmed_id': confirmed_id.id})

    def return_picking(self, picking):
        if picking.state == 'draft':
            picking.unlink()
            return True
        wizard = self.env['stock.return.picking']
        config = wizard.with_context(
            active_id=picking.id, active_ids=picking.ids,
            active_model='stock.picking').default_get(
                list(wizard.fields_get())
        )
        wiz_id = wizard.create({'invoice_state': 'none'})
        if config.get('product_return_moves', False):
            for move_dict in config['product_return_moves']:
                if move_dict.get('move_id', False) in picking.move_lines.ids:
                    move_dict.update({'quantity': abs(picking.move_lines.filtered(
                            lambda y: y.id == move_dict['move_id']
                    ).product_uom_qty)})
        wiz_id.update(config)
        res = wiz_id.with_context(active_id=picking.id).create_returns()
        domain = safe_eval(res.get('domain'))
        picking_id = self.env['stock.picking'].search(domain)
        picking_id.write({'date': picking.date})
        picking_id.action_confirm()
        picking_id.force_assign()
        self.env['stock.move'].browse([
            x.id for x in picking_id.move_lines if x.state != 'done']
        ).with_context(date_mrp=picking.date).action_done()
        return True

    @api.multi
    def action_cancel(self):
        res = super(MrpRepair, self).action_cancel()
        for repair in self:
            # if repairable product is received, create return picking
            if repair.out_picking_id:
                if repair.out_picking_id.state != 'done':
                    repair.out_picking_id.action_cancel()
                    repair.write({'state': 'cancel'})
                else:
                    raise Warning(_('Repair cannot be cancelled!'))
            elif repair.in_picking_id and repair.in_picking_id.state != 'cancel':
                self.return_picking(repair.in_picking_id)
        return res

    @api.multi
    def action_wait_confirmation(self):
        #  save current revision of mrp.repair to use as history 'quotation'
        for repair in self:
            repair.write({'state': 'wait_confirmation'})
            repair.save_mrp_repair_sent()
        return {}

    @api.multi
    def save_mrp_repair_sent(self):
        # Create a copy of repair and mark it cancelled
        # change name to number + x revision
        revno = self.env['mrp.repair'].search([
            ('original_name', '=', self.name)
        ], order='revision_nr desc', limit=1)
        defaults = {
            'mrp_repair_parent_id': self.id,
            'original_name': self.name,
            'revision_nr': revno[0].revision_nr + 1 if revno else 1,
            'state': 'cancel',
            'date': fields.Datetime.now(),
            'name': '%s-%02d' % (self.name,
                                 revno[0].revision_nr + 1 if revno else 1),
        }
        self.copy(defaults)

    @api.multi
    def income_picking_create(self):
        stock_move_obj = self.env['stock.move']
        picking_id = False
        for repair_order in self:
            if repair_order.in_picking_id:
                picking_id = repair_order.in_picking_id.id
            else:
                picking_id = self.env['stock.picking'].create({
                    'name': 'IN/' + repair_order.name,
                    'origin': repair_order.name,
                    'partner_id': repair_order.address_id.id if
                    repair_order.address_id else repair_order.partner_id.id if
                    repair_order.partner_id else False,  # ?
                    'invoice_state': 'none',
                    'company_id': repair_order.company_id.id,
                    'move_lines': [],
                    'picking_type_id': self.env.ref(
                        'stock.picking_type_in').id
                })

                if repair_order.product_id:
                    move_vals = {
                        'name': repair_order.name,
                        'product_id': repair_order.product_id.id,
                        'product_uom_qty': 1,
                        'partner_id': repair_order.partner_id.id if
                        repair_order.partner_id else False,
                        'product_uom': repair_order.product_id.uom_id.id,
                        'date': fields.Datetime.now(),
                        'location_id': repair_order.address_id.
                        property_stock_customer.id,
                        'location_dest_id': self.env.ref(
                            'mrp_repair_management.stock_location_repair_in'
                        ).id,
                        'picking_id': picking_id.id,
                        'state': 'draft',
                        'company_id': repair_order.company_id.id,
                        'restrict_lot_id': repair_order.lot_id.id,
                    }
                    move = stock_move_obj.create(move_vals)
                    move.action_confirm()
                    move.force_assign()
                    workflow.trg_validate(
                        self._uid, 'stock.picking',
                        picking_id.id, 'button_confirm', self._cr)
                    # picking_id.do_enter_transfer_details() TODO come fare
        return picking_id

    @api.multi
    def action_finish(self):
        for repair_order in self:
            repair_order.out_products_picking_create()
            if not repair_order.out_products_picking_id and \
                    repair_order.operations.filtered('product_id'):
                raise Warning(_(
                    'Missing outgo products move for repair order %s! '
                    'Please signal to support team.' % repair_order.name))
            repair_order.write({'state': 'done'})
        return {}

    @api.multi
    def action_return_machine(self):
        for repair_order in self:
            if repair_order.in_picking_id and not repair_order.out_picking_id:
                out_picking_id = repair_order.out_picking_create()
                if out_picking_id:
                    out_sppp_id = repair_order.create_outgo_sppp(
                        out_picking_id)
                    if out_sppp_id:
                        repair_order.write({
                            'out_sppp_id': out_sppp_id.id,
                        })
                    repair_order.write({'out_picking_id': out_picking_id.id,
                                        'state': 'wait_outgo'})

    @api.multi
    def out_picking_create(self):
        repair_order = self
        defaults = {
            'name': repair_order.name + '/OUT',
            'origin': repair_order.name,
            'partner_id': repair_order.address_id.id if
            repair_order.address_id else repair_order.partner_id.id if
            repair_order.partner_id else False,  # ?
            'invoice_state': 'none',
            'company_id': repair_order.company_id.id,
            'picking_type_id': self.env.ref(
                'stock.picking_type_out').id
        }
        out_picking_id = self.in_picking_id.copy(defaults)
        out_picking_id.move_lines.write({
            'location_id': self.env.ref(
                'mrp_repair_management.stock_location_repair_in').id,
            'location_dest_id': repair_order.address_id and
            repair_order.address_id.property_stock_customer.id or False,
            'restrict_lot_id': repair_order.lot_id.id,
        })
        for move in out_picking_id.move_lines:
            move.action_confirm()
            move.force_assign()
        workflow.trg_validate(
            self._uid, 'stock.picking',
            out_picking_id.id, 'button_confirm', self._cr)
        repair_order.write({
            'state': 'done', 'out_picking_id': out_picking_id.id})
        return out_picking_id

    @staticmethod
    def _prepare_stock_move_vals(op, date):
        return {
            'name': op.name,
            'product_id': op.product_id.id,
            'restrict_lot_id': op.lot_id.id,
            'product_uom_qty': op.product_uom_qty * (-1 if op.type == 'remove' else 1),
            'product_uom': op.product_uom.id,
            'partner_id': op.repair_id.address_id and op.repair_id.address_id.id
            or False,
            'invoice_state': 'none',  # create invoice from repair only
            'origin': op.repair_id.name,
            'date': date,
            'mrp_repair_line_id': op.id,
        }

    @api.multi
    def update_out_moves(self, date=False):
        """ Creates stock move for operations from stock to repair in progress. """
        move_obj = self.env['stock.move']
        for repair in self:
            for operation in repair.operations.filtered(
                lambda x: not x.move_id and x.product_id.type != 'service'
            ):
                move_vals = self._prepare_stock_move_vals(
                    operation, date or operation.create_date)
                location_dest_id = self.env.ref(
                    'mrp_repair_management.stock_location_repair_progress')
                location_id = operation.location_id
                if operation.type == 'remove':
                    location_id = self.env.ref('stock.picking_type_out').\
                        default_location_dest_id
                    location_dest_id = operation.location_dest_id
                move_vals.update({
                    'location_id': location_id.id,
                    'location_dest_id': location_dest_id.id,
                })
                move_id = move_obj.create(move_vals)
                move_id.with_context(date_mrp=date or operation.create_date).\
                    action_done()
                operation.write({'move_id': move_id.id, 'state': 'done'})
            # set next state for repair with out picking or on site
            if repair.on_site and not self.env.context.get('bypass_state_update'):
                repair.write({'state': '2binvoiced'})
            if repair.out_picking_id and (
                repair.machine_id.ownership != 'own' or
                repair.partner_id != self.env.user.company_id.partner_id
            ) and not self.env.context.get('bypass_state_update'):
                repair.write({'state': '2binvoiced'})

        return True

    @api.multi
    def out_products_picking_create(self):
        #  Creates stock move only for final products of repair order.
        picking_type_id = self.env.ref('stock.picking_type_out')
        move_obj = self.env['stock.move']
        date_mrp = self.out_sppp_id.date if self.out_sppp_id else \
            self.date
        picking_id = self.env['stock.picking'].create({
            'origin': self.name,
            'partner_id': self.address_id.id if
            self.address_id else self.partner_id.id if
            self.partner_id else False,
            'invoice_state': 'none',  # create invoice only from repair
            'company_id': self.company_id.id,
            'move_lines': [],
            'picking_type_id': picking_type_id.id,
            'date': date_mrp,
        })
        self.update({'out_products_picking_id': picking_id.id})
        for operation in self.operations.filtered(
                lambda y: y.product_id.type != 'service'):
            date = operation.repair_id.out_sppp_id.date \
                if operation.repair_id.out_sppp_id else \
                operation.repair_id.date
            move_vals = self._prepare_stock_move_vals(operation, date)
            location_id = operation.location_id
            location_dest_id = picking_type_id.default_location_dest_id
            if operation.move_id:
                location_id = operation.move_id.location_dest_id
            if operation.type == 'remove':
                if operation.move_id:
                    location_id = operation.location_dest_id
                    location_dest_id = picking_type_id.default_location_dest_id
                else:
                    location_id = picking_type_id.default_location_dest_id
                    location_dest_id = operation.location_dest_id
            move_vals.update({
                'location_id': location_id.id,
                'location_dest_id': location_dest_id.id,
                'picking_id': picking_id.id,
            })
            self.out_products_picking_id.move_lines |= self.env['stock.move'].create(
                move_vals
            )
        picking_id.action_confirm()
        picking_id.force_assign()
        move_obj.browse([
            x.id for x in picking_id.move_lines if x.state != 'done']
        ).with_context(date_mrp=date_mrp).action_done()
        picking_id.write({'date_done': date_mrp})
        return picking_id

    @api.multi
    def create_outgo_sppp(self, picking_id):
        sppp_obj = self.env['stock.picking.package.preparation']
        ddt_type_id = self.env.ref(
            'stock_ddt_data.ddt_type_ddt_reso_criparazione')
        sppp_data = {
            'partner_id': self.partner_id.id,
            'partner_invoice_id': self.partner_id.id,
            'partner_shipping_id': self.partner_id.id,
            'picking_ids': [(6, 0, picking_id.ids)],
            'ddt_type_id': ddt_type_id.id,
            'carriage_condition_id': ddt_type_id.carriage_condition_id.id,
            'goods_description_id': ddt_type_id.goods_description_id.id,
            'transportation_reason_id':
            ddt_type_id.transportation_reason_id.id,
            'transportation_method_id':
            ddt_type_id.transportation_method_id.id,
        }
        sppp_id = sppp_obj.create(sppp_data)
        return sppp_id

    @api.multi
    def action_invoice_create(self, group=False):
        res = super(MrpRepair, self).action_invoice_create(group)
        payment_term = self.payment_term if self.payment_term else \
            self.partner_invoice_id.property_payment_term if \
            self.partner_invoice_id.property_payment_term else False
        if payment_term:
            for invoice in self.env['account.invoice'].browse(res[self.id]):
                invoice.write({'payment_term': payment_term.id})
        for repair in self:
            repair.out_sppp_id.write({'invoice_id': res[self.id]})
        return res

    @api.multi
    def unlink(self):
        raise Warning(_('You cannot delete repair!'))

    @api.multi
    def action_repaired(self):
        for order in self:
            not order.on_site and order.write({'state': 'wait_delivery'})\
                or order.on_site and order.write({'state': 'wait_outgo'})
        return True
