# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, models, fields


class MachineRepair(models.Model):
    _name = "machine.repair"
    _inherit = "machinery"

    partner_id = fields.Many2one(
        'res.partner',
        string='Partner that own this machine')
    ownership = fields.Selection(
        selection_add=[('partner', 'Partner')],
        default='partner',)
    product = fields.Many2one(
        required=True,
        default=lambda self: self.env.ref(
            'mrp_repair_machine.product_product_repair_generic'))
    plate = fields.Char()
    date_start = fields.Date()
    note = fields.Char()

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        ids = self.search([('frame', 'ilike', name)] + args,
                          limit=limit)
        if ids:
            return ids.name_get()
        return super(MachineRepair, self).name_search(
            name=name, args=args, operator=operator, limit=limit)

    @api.onchange('ownership')
    def onchange_ownership(self):
        if self.ownership == 'own':
            self.partner_id = self.env.user.company_id.partner_id


class ProductTemplate(models.Model):
    _inherit = "product.template"

    machine_repair_ok = fields.Boolean(
        'Can be a repairable Machine', help="Determines if the "
        "product is related with a repairable machine.")
