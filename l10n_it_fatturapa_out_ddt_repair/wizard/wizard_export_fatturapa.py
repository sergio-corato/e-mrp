# -*- coding: utf-8 -*-
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).

from openerp import models, fields, api, _
from openerp.exceptions import Warning as UserError
from openerp.addons.l10n_it_fatturapa.bindings.fatturapa import (
    DatiDDTType,
    DatiTrasportoType,
    DatiAnagraficiVettoreType,
    IdFiscaleType,
    AnagraficaType
)


class WizardExportFatturapa(models.TransientModel):
    _inherit = "wizard.export.fatturapa"

    def setDatiDDT(self, invoice, body):
        res = super(WizardExportFatturapa, self).setDatiDDT(
            invoice, body)
        if self.include_ddt_data == 'dati_ddt':
            mrp = False
            mrp_onsite = False
            rental_ddt = False
            rental_ddt_date = False
            rental_end_date = False
            caus_list = {}
            for line in invoice.invoice_line:
                sale_order_name = False
                if invoice.stock_picking_package_preparation_ids:
                    for picking in invoice.picking_ids:
                        # add mrp_repair datas
                        mrp = self.env['mrp.repair'].search(
                            [('out_picking_id', '=', picking.id)])
                        if mrp:
                            key = (
                                picking.ddt_ids[0].ddt_number,
                                picking.ddt_ids[0].date[:10]
                            )
                            sale_order_name = picking.origin
                            if key not in caus_list:
                                caus_list[key] = {
                                    'mrp_name': mrp.name,
                                    'mrp_date': mrp.date[:10],
                                    'mrp_machine': mrp.machine_id.name,
                                    'mrp_frame': mrp.machine_id.frame if
                                    mrp.machine_id.frame else 'n.d.',
                                }
                else:
                    sale_order_name = line.origin
                sale_order = self.env['sale.order'].search([
                    ('name', '=', sale_order_name)
                ], limit=1)
                if sale_order:
                    # add sale rental data if exists
                    for order_line in sale_order.order_line:
                        if order_line.rental_type and \
                                order_line.order_id.ddt_ids:
                            rental_ddt = order_line.order_id.ddt_ids[
                                0].ddt_number
                            rental_ddt_date = order_line.order_id.ddt_ids[
                                0].date[:10]
                            pick_type_in = self.env['ir.model.data']. \
                                get_object_reference(
                                'stock', 'picking_type_in')[1]
                            rental_pick = [
                                x for x in
                                order_line.order_id.picking_ids if
                                x.picking_type_id.id ==
                                pick_type_in]
                            if rental_pick and rental_pick[0].date_done:
                                rental_end_date = rental_pick[0].date_done[:10]
                            break
                    if rental_ddt:
                        caus_list[(line.origin, 'rental')] = {
                            'rental_ddt': rental_ddt,
                            'rental_ddt_date': rental_ddt_date,
                            'rental_end_date': rental_end_date,
                        }
                # search mrp without out_picking_id (onsite)
                mrp_onsite = self.env['mrp.repair'].search([
                    ('name', '=', line.origin)
                ])
                if mrp_onsite and not mrp:
                    caus_list[(line.origin, 'onsite')] = {
                        'mrp_name': mrp_onsite.name,
                        'mrp_date': mrp_onsite.date[:10],
                        'mrp_machine': mrp_onsite.machine_id.name,
                        'mrp_frame': mrp_onsite.machine_id.frame if
                        mrp_onsite.machine_id.frame else 'n.d.',
                    }

            if mrp or mrp_onsite or rental_ddt:
                for caus in caus_list:
                    if not caus:
                        continue
                    if mrp and caus_list[caus].get('mrp_name', False):
                        causale = _('Our Ref. Picking %s dated %s. '
                                    'Ref. Our Order %s dated %s. '
                                    'Machine: %s frame %s.') % (
                            caus[0], caus[1],
                            caus_list[caus]['mrp_name'],
                            caus_list[caus]['mrp_date'],
                            caus_list[caus]['mrp_machine'],
                            caus_list[caus]['mrp_frame'],
                        )
                    elif mrp_onsite and caus_list[caus].get('mrp_name', False):
                        causale = _('Ref. Our Order %s dated %s. '
                                    'Machine: %s frame %s.') % (
                            caus_list[caus]['mrp_name'],
                            caus_list[caus]['mrp_date'],
                            caus_list[caus]['mrp_machine'],
                            caus_list[caus]['mrp_frame'],
                        )
                    elif rental_ddt and caus_list[caus].get('rental_ddt', False):
                        causale = _('Outgo document: %s dated %s. '
                                    'Date return %s.') % (
                            caus_list[caus]['rental_ddt'],
                            caus_list[caus]['rental_ddt_date'],
                            caus_list[caus]['rental_end_date'],
                        )
                    causale_list_200 = \
                        [causale[i:i + 200] for i in
                         range(0, len(causale), 200)]
                    for causale200 in causale_list_200:
                        # Remove non latin chars, but go back to unicode string
                        # as expected by String200LatinType
                        causale = causale200.encode(
                            'latin', 'ignore').decode('latin')
                        body.DatiGenerali.DatiGeneraliDocumento.Causale \
                            .append(causale)
        return res

    def prepareRelDocsLine(self, invoice, line):
        res = super(WizardExportFatturapa, self).prepareRelDocsLine(
            invoice, line)
        if not res:
            sale_order_name = False
            if line.origin:
                # if invoiced from picking, get sale_order from picking
                for picking in invoice.picking_ids:
                    if picking.name == line.origin:
                        sale_order_name = picking.origin
                        break
                # else use origin directly
                if not sale_order_name:
                    sale_order_name = line.origin
                # add mrp_repair as order
                order = self.env['mrp.repair'].search(
                    [('name', '=', sale_order_name)])
                if order:
                    name = order.name
                    res = {
                        'name': name[:20].replace('\n', ' ').replace
                        ('\t', ' ').replace('\r', ' ').encode(
                            'latin', 'ignore').decode('latin'),
                        'date': order.date[:10],
                    }
        return res
