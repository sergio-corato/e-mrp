# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT


class PhoneCommon(models.AbstractModel):
    _inherit = 'phone.common'

    @api.model
    def _prepare_incall_pop_action(self, record_res, number):
        action = False
        vals = {
            'date': fields.Datetime.now(),
            'name': _('Incoming call from %s') % record_res[2] if record_res
            else _('Number Not Found'),
            'partner_id': record_res[1] if record_res and
            record_res[0] == 'res.partner' else '',
            'user_id': False,
            'opportunity_id': record_res[1] if record_res and
            record_res[0] == 'crm.lead' else '',
            'partner_phone': number,
        }
        try:
            crm_phonecall_id = self.env['crm.phonecall'].create(vals)
            if crm_phonecall_id:
                action = {
                    'name': 'Phone call',
                    'type': 'ir.actions.act_window',
                    'res_model': 'crm.phonecall',
                    'view_mode': 'form,tree',
                    'views': [[False, 'form']],  # Beurk, but needed
                    'target': 'new',
                    'flags': {'form': {'action_buttons': True}},
                    'res_id': crm_phonecall_id.id,
                }
        except:
            pass

        return action

    @api.model
    def close_call_from_phone_number(self, presented_number):
        # Function to set end time to the call from phone number.
        res = self.get_record_from_phone_number(presented_number)
        if res:
            # search if there is a call without ending time for this partner or
            # lead
            obj = res[0]
            obj_id = res[1]
            phonecall_obj = self.env['crm.phonecall']
            call_id = phonecall_obj.search([
                ('partner_id' if obj == 'res.partner' else 'opportunity_id',
                 '=', obj_id), ('duration', '=', False)],
                order='date DESC', limit=1)
            if call_id:
                duration = fields.datetime.now() - \
                                   datetime.strptime(
                                       call_id.date,
                                       DEFAULT_SERVER_DATETIME_FORMAT)
                call_id.duration = (duration.total_seconds() / 60)
                return True
            return False
        else:
            return False
