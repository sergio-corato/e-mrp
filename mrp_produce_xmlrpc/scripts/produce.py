import erppeek
import sys
from optparse import OptionParser

__author__ = "Sergio Corato"
__date__ = "August 2017"
__version__ = "0.1"


# Define command line options
options = [
    {'names': ('-s', '--server'), 'dest': 'server', 'type': 'string',
        'action': 'store', 'default': False,
        'help': 'DNS or IP address of the OpenERP server. Default = none '
        '(will not try to connect to OpenERP)'},
    {'names': ('-e', '--ssl'), 'dest': 'ssl',
        'help': "Use SSL connections instead of clear connections. "
        "Default = no, use clear XML-RPC",
        'action': 'store_true', 'default': False},
    {'names': ('-p', '--port'), 'dest': 'port', 'type': 'int',
        'action': 'store', 'default': False,
        'help': "Port of OpenERP's XML-RPC interface. Default = 8069"},
    {'names': ('-d', '--database'), 'dest': 'database', 'type': 'string',
        'action': 'store', 'default': False,
        'help': "Odoo database name. Default = False"},
    {'names': ('-t', '--username'), 'dest': 'username', 'type': 'string',
        'action': 'store', 'default': 'admin',
        'help': "OpenERP username to use when connecting to Odoo. "
        "Default = admin"},
    {'names': ('-w', '--password'), 'dest': 'password', 'type': 'string',
        'action': 'store', 'default': 'demo',
        'help': "Password of the OpenERP user. Default = 'demo'"},
    {'names': ('-m', '--production-mo'), 'dest': 'production_mo', 'type':
     'string', 'action': 'store', 'default': False,
     'help': "Production Order name. Default = False"},
    {'names': ('-l', '--lot'), 'dest': 'lot', 'type': 'string',
     'action': 'store', 'default': False,
     'help': "Lot name. Default = False"},
]
#python ./produce.py --server https://2045tech.efatto.it -e --database 2045test --username produzione --password prod --production-mo  MO00005 --lot X000001
#python ./produce.py --server http://localhost --port 8069 --database 2045tech --username produzione --password prod --production-mo  MO00007 --lot B000004

def main(options, arguments):
    if options.server:
        print(
            'VERBOSE "Starting XML-RPC request on Odoo %s:%d '
            'database %s user %s"\n' % (
                options.server, options.port, options.database,
                options.username))
        server = options.server
        if not options.ssl and options.port:
            server = options.server + ':' + str(options.port)

        try:
            client = erppeek.Client(
                server=server,
                db=options.database,
                user=options.username,
                password=options.password,
            )
        except:
            print('VERBOSE "Could not connect to Odoo in XML-RPC"\n')

        try:
            if options.production_mo and options.lot:
                res = client.MrpProduction.produce_lot_remote(
                    options.production_mo, options.lot)
                print('VERBOSE "%s"\n' % res)
            else:
                print('VERBOSE Production_mo or lot not passed to function.\n')
        except:
            print('VERBOSE "Could not produce."\n')

    return {}


if __name__ == '__main__':
    usage = "Usage: produce.py [options]"
    epilog = "Script written by Sergio Corato. "
    "Published under the GNU AGPL licence."
    description = "This is a script that sends a query to Odoo to produce " \
                  "product of a manifacturing order. "
    parser = OptionParser(usage=usage, epilog=epilog, description=description)
    for option in options:
        param = option['names']
        del option['names']
        parser.add_option(*param, **option)
    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)

